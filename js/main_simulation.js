function disableDotLight()
{
    world.pbr.updateDotLight(0, {color: [0.0]})
}
function disableSpotLight()
{
    world.pbr.updateSpotLight(0, {color: [0.0]})
}
function disableDirLight()
{
    world.pbr.updateDirLight(0, {color: [0.0]})
}

function updateDotLight()
{
    if(!world.pbr)
        world.pbr = world.getIllumination(0)

    disableDirLight()
    disableSpotLight()
    world.pbr.updateDotLight(0, {position:
                                     [world.cameras.cam0.x,
                                     world.cameras.cam0.y,
                                     world.cameras.cam0.z],
                                 color: [1.5]
                             })
}
function updateSpotLight()
{
    if(!world.pbr)
        world.pbr = world.getIllumination(0)

    disableDirLight()
    disableDotLight()
    world.pbr.updateSpotLight(0, {direction:
                                      [world.cameras.cam0.viewDirX,
                                      world.cameras.cam0.viewDirY,
                                      world.cameras.cam0.viewDirZ],
                                  position:
                                      [world.cameras.cam0.x,
                                      world.cameras.cam0.y,
                                      world.cameras.cam0.z],
                                  color: [1.5]
                              })
}
function updateDirLight()
{
    if(!world.pbr)
        world.pbr = world.getIllumination(0)

    disableDotLight()
    disableSpotLight()
    world.pbr.updateDirLight(0, {direction:
                                     [world.cameras.cam0.viewDirX,
                                     world.cameras.cam0.viewDirY,
                                     world.cameras.cam0.viewDirZ],
                                 color: [1.5]
                             })
}

function onContact(e0, e1)
{
    console.log("onContact", e0, e1)
}

function onContactLost(e0, e1)
{
    console.log("onContactLost", e0, e1)
}

function onHasContactsChanged(e)
{
    if(e)
    {
        if(e.hasContacts)
            e.body.setProperty("material", "texture_collision")
        else
            e.body.setProperty("material", "texture")
    }
}

const models = ["cube", "sphere", "torus"]
var i = 1
function addEntity()
{
    const model = models[Math.round(Math.random() * (models.length-1))]
    const material = "texture"
    const id = "Object_" + i.toString()
    let pos = [
            world.cameras.cam0.x,
            world.cameras.cam0.y,
            world.cameras.cam0.z
        ]

    world.entities[id] = world.createEntity({
                                                "type": "rigid_model_body",
                                                "material": material,
                                                "model": model
                                            }, id)

    world.entities[id].x = pos[0]
    world.entities[id].y = pos[1]
    world.entities[id].z = pos[2]
    world.entities[id].contact.connect(onContact)
    world.entities[id].contactLost.connect(onContactLost)
    world.entities[id].hasContactsChanged.connect(function() { onHasContactsChanged(world.entities[id]) })
    ++i
}

world.addInput({
                   "type": "keyboard",
                   "bindings": [
                       { "code": Qt.Key_F1,   "on_released": updateDotLight },
                       { "code": Qt.Key_F2,   "on_released": updateSpotLight },
                       { "code": Qt.Key_F3,   "on_released": updateDirLight },
                       { "code": Qt.Key_F4,   "on_released": addEntity }
                   ]
               })

world.setLocation(world.locations.loc0)

if(!world.entities)
    world.entities = {
        "Object_0": world.getEntity("Object_0")
    }

world.entities["Object_0"].contact.connect(onContact)
world.entities["Object_0"].contactLost.connect(onContactLost)
world.entities["Object_0"].hasContactsChanged.connect(function() { onHasContactsChanged(world.entities["Object_0"]) })
