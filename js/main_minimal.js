const cam_accel = 2.0

let mouseCatched = true
function switchMouseCatched()
{
    if(mouseCatched)
        world.disableInput(world.relPointerInputIndex)
    else
        world.enableInput(world.relPointerInputIndex)
    mouseCatched = !mouseCatched;
}

function rotateCamera(deltaX, deltaY)
{
    world.cameras.cam0.ox += deltaY / 16.0
    world.cameras.cam0.oy -= deltaX / 16.0

    if(world.cameras.cam0.ox >= 90)
        world.cameras.cam0.ox = 89.9

    if(world.cameras.cam0.ox <= -90)
        world.cameras.cam0.ox = -89.9

    world.cameras.cam0.setAngles(world.cameras.cam0.ox, world.cameras.cam0.oy, 0)

}

world.cameras.cam0.ox = 0.0
world.cameras.cam0.oy = 0.0
world.relPointerInputIndex = world.addInput({"type": "relative_pointer", "func": rotateCamera})
world.addInput({
                   "type": "keyboard",
                   "bindings": [
                       { "code": Qt.Key_Control, "on_released": switchMouseCatched }
                   ]
               })

