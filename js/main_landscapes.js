const cam_accel = 2.0
let locations = ["loc0", "loc1"]
let location_index = 0

let mouseCatched = true
function switchMouseCatched()
{
    if(mouseCatched)
        world.disableInput(world.relPointerInputIndex)
    else
        world.enableInput(world.relPointerInputIndex)
    mouseCatched = !mouseCatched;
}

function disableDotLight()
{
    world.pbr.updateDotLight(0, {color: [0.0]})
}
function disableSpotLight()
{
    world.pbr.updateSpotLight(0, {color: [0.0]})
}
function disableDirLight()
{
    world.pbr.updateDirLight(0, {color: [0.0]})
}

function updateDotLight()
{
    if(!world.pbr)
        world.pbr = world.getIllumination(0)

    disableDirLight()
    disableSpotLight()
    world.pbr.updateDotLight(0, {position:
                                     [world.cameras.cam0.x,
                                     world.cameras.cam0.y,
                                     world.cameras.cam0.z],
                                 color: [1.5]
                             })
}
function updateSpotLight()
{
    if(!world.pbr)
        world.pbr = world.getIllumination(0)

    disableDirLight()
    disableDotLight()
    world.pbr.updateSpotLight(0, {direction:
                                      [world.cameras.cam0.viewDirX,
                                      world.cameras.cam0.viewDirY,
                                      world.cameras.cam0.viewDirZ],
                                  position:
                                      [world.cameras.cam0.x,
                                      world.cameras.cam0.y,
                                      world.cameras.cam0.z],
                                  color: [1.5]
                              })
}
function updateDirLight()
{
    if(!world.pbr)
        world.pbr = world.getIllumination(0)

    disableDotLight()
    disableSpotLight()
    world.pbr.updateDirLight(0, {direction:
                                     [world.cameras.cam0.viewDirX,
                                     world.cameras.cam0.viewDirY,
                                     world.cameras.cam0.viewDirZ],
                                 color: [1.5]
                             })
}

function rotateCamera(deltaX, deltaY)
{
    world.cameras.cam0.ox += deltaY / 16.0
    world.cameras.cam0.oy -= deltaX / 16.0

    if(world.cameras.cam0.ox >= 90)
        world.cameras.cam0.ox = 89.9

    if(world.cameras.cam0.ox <= -90)
        world.cameras.cam0.ox = -89.9

    world.cameras.cam0.setAngles(world.cameras.cam0.ox, world.cameras.cam0.oy, 0)

}

function camLeft(sec_elapsed)
{
    world.cameras.cam0.x_vel += cam_accel
}
function camRight(sec_elapsed)
{
    world.cameras.cam0.x_vel -= cam_accel
}
function camForward(sec_elapsed)
{
    world.cameras.cam0.z_vel += cam_accel
}
function camBack(sec_elapsed)
{
    world.cameras.cam0.z_vel -= cam_accel
}
function camUp(sec_elapsed)
{
    world.cameras.cam0.y_vel += cam_accel
}
function camDown(sec_elapsed)
{
    world.cameras.cam0.y_vel -= cam_accel
}
function keyboardPostTick(sec_elapsed)
{
    if(Math.abs(world.cameras.cam0.x_vel) <= 0.001
            && Math.abs(world.cameras.cam0.y_vel) <= 0.001
            && Math.abs(world.cameras.cam0.z_vel) <= 0.001) return;

    let tr_vec = world.rotatedVector(world.cameras.cam0.x_vel, world.cameras.cam0.y_vel, world.cameras.cam0.z_vel,
                                     world.cameras.cam0.ox, world.cameras.cam0.oy, 0)
    world.cameras.cam0.translate(tr_vec[0] * sec_elapsed, tr_vec[1] * sec_elapsed, tr_vec[2] * sec_elapsed)
    world.cameras.cam0.x_vel = world.cameras.cam0.y_vel = world.cameras.cam0.z_vel = 0
}

function switchLocation()
{
    location_index++
    location_index %= locations.length
    world.setLocation(world.locations[locations[location_index]])
}

world.cameras.cam0.ox = 0.0
world.cameras.cam0.oy = 0.0
world.cameras.cam0.x_vel = world.cameras.cam0.y_vel = world.cameras.cam0.z_vel = 0
world.relPointerInputIndex = world.addInput({"type": "relative_pointer", "func": rotateCamera})

world.addInput({
                   "type": "keyboard",
                   "post_tick": keyboardPostTick,
                   "bindings": [
                       { "text": "ф", "on_tick": camLeft },
                       { "text": "в", "on_tick": camRight },
                       { "text": "ц", "on_tick": camForward },
                       { "text": "ы", "on_tick": camBack },

                       { "text": "Ф", "on_tick": camLeft },
                       { "text": "В", "on_tick": camRight },
                       { "text": "Ц", "on_tick": camForward },
                       { "text": "Ы", "on_tick": camBack },

                       { "code": Qt.Key_A, "on_tick": camLeft },
                       { "code": Qt.Key_D, "on_tick": camRight },
                       { "code": Qt.Key_W, "on_tick": camForward },
                       { "code": Qt.Key_S, "on_tick": camBack },

                       { "code": Qt.Key_Left,  "on_tick": camLeft },
                       { "code": Qt.Key_Right, "on_tick": camRight },
                       { "code": Qt.Key_Up,    "on_tick": camForward },
                       { "code": Qt.Key_Down,  "on_tick": camBack },

                       { "code": Qt.Key_Shift, "on_tick": camDown },
                       { "code": Qt.Key_Space, "on_tick": camUp },

                       { "code": Qt.Key_Control, "on_released": switchMouseCatched },
                       { "code": Qt.Key_Tab, "on_released": switchLocation },

                       { "code": Qt.Key_F1,   "on_released": updateDotLight },
                       { "code": Qt.Key_F2,   "on_released": updateSpotLight },
                       { "code": Qt.Key_F3,   "on_released": updateDirLight }
                   ]
               })

world.setLocation(world.locations.loc0)
