import QtQuick 2.4

Item {
    anchors.fill: parent
    id: item

    Text {
        id: brightnessText
        x: 5
        y: 5
        z: 1
        text: "Low brightness"
        color: "gray"
        styleColor: "black"
        style: Text.Outline
        font.pixelSize: 52

        MouseArea {
            anchors.fill: parent
            property bool bright: true

            onClicked: {
                if(!bright)
                {
                    brightnessText.text = "High brightness"
                    brightnessText.color = "yellow"
                    world.resources.exposure = 1.5
                    world.resources.gamma = 2.8
                } else
                {
                    brightnessText.text = "Low brightness"
                    brightnessText.color = "gray"
                    world.resources.exposure = 0.75
                    world.resources.gamma = 2.6
                }
                bright = !bright
            }
        }
    }

    Text {
        id: controlsText
        anchors.right: parent.right
        y: 5
        text: `Press <b>F1</b> to set a dot light<br>
        Press <b>F2</b> to set a spot light<br>
        Press <b>F3</b> to set a dir light<br>
        Press <b>F4</b> to add a new rigid object<br>
        Hold <b>RMB</b> to look around<br>
        Hold <b>LMB</b> to move horizontally<br>
        Hold <b>Middle mouse btn</b> to move vertically`
        color: "white"
        styleColor: "black"
        style: Text.Outline
        font.pixelSize: 25
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton | Qt.MiddleButton

        property real lastMouseX
        property real lastMouseY

        readonly property real k : 0.01

        property var pressedEntity: null

        function moveMouse() {
            const deltaX = mouseX - lastMouseX
            const deltaY = mouseY - lastMouseY

            if(!world.cameras.cam0.ox || !world.cameras.cam0.oy) {
                world.cameras.cam0.ox = 0.0
                world.cameras.cam0.oy = 0.0
            }

            let tr = null
            if(pressedButtons & Qt.LeftButton) {
                tr = world.rotatedVector(deltaX*k, 0, deltaY*k,
                                            world.cameras.cam0.ox, world.cameras.cam0.oy, 0)
            }
            else if(pressedButtons & Qt.MiddleButton) {
                tr = world.rotatedVector(deltaX*k, deltaY*k, 0,
                                            world.cameras.cam0.ox, world.cameras.cam0.oy, 0)
            }
            else if(pressedButtons & Qt.RightButton) {
                world.cameras.cam0.ox += deltaY / 16.0
                world.cameras.cam0.oy -= deltaX / 16.0

                if(world.cameras.cam0.ox >= 90)
                    world.cameras.cam0.ox = 89.9

                if(world.cameras.cam0.ox <= -90)
                    world.cameras.cam0.ox = -89.9

                world.cameras.cam0.setAngles(world.cameras.cam0.ox, world.cameras.cam0.oy, 0)
            }
            if(tr) {
                if(pressedEntity) {
                    if(world.getGearbox().getProperty("useBulletSimulation")) {
                        pressedEntity.vel_x -= tr[0] * 6
                        pressedEntity.vel_y -= tr[1] * 6
                        pressedEntity.vel_z -= tr[2] * 6
                    } else {
                        pressedEntity.x -= tr[0]
                        pressedEntity.y -= tr[1]
                        pressedEntity.z -= tr[2]
                    }
                } else
                    world.cameras.cam0.translate(tr[0], tr[1], tr[2])
            }

            lastMouseX = mouseX
            lastMouseY = mouseY
        }

        onMouseXChanged: moveMouse()
        onMouseYChanged: moveMouse()

        onPressed: {
            lastMouseX = mouseX
            lastMouseY = mouseY

            pressedEntity = world.getEntityAt(mouseX, mouseY)
            const material = pressedEntity? pressedEntity.body.getProperty("material"): null
            if(material === "texture")
                pressedEntity.body.setProperty("material", "texture_pressed")
            else if(material !== "texture_pressed" && material !== "texture_collision")
                pressedEntity = null
        }

        onReleased: {
            if(pressedEntity && pressedEntity.body.getProperty("material") === "texture_pressed") {
                pressedEntity.body.setProperty("material", "texture")
            }
            pressedEntity = null
        }

        onDoubleClicked: {
            let e = world.getEntityAt(mouseX, mouseY)
            const material = e? e.body.getProperty("material"): null
            if(material !== "texture" && material !== "texture_pressed" && material !== "texture_collision")
                return
            e.remove()
        }
    }
}
