import QtQuick 2.0

Item {
    anchors.fill: parent
    id: item

    Text {
        id: brightnessText
        x: 5
        y: 5
        text: "Low brightness"
        color: "gray"
        styleColor: "black"
        style: Text.Outline
        font.pixelSize: 52

        MouseArea {
            anchors.fill: parent
            property bool bright: true

            onClicked: {
                if(!world.pbr)
                    world.pbr = world.getIllumination(0)
                if(!bright)
                {
                    brightnessText.text = "High brightness"
                    brightnessText.color = "yellow"
                    world.pbr.updateDirLight(0, { ambient: [0.6] })
                } else
                {
                    brightnessText.text = "Low brightness"
                    brightnessText.color = "gray"
                    world.pbr.updateDirLight(0, { ambient: [0.3] })
                }
                bright = !bright
            }
        }
    }

    Text {
        id: controlsText
        anchors.right: parent.right
        y: 5
        text: `Press <b>F1</b> to set a dot light<br>
        Press <b>F2</b> to set a spot light<br>
        Press <b>F3</b> to set a dir light<br>
        Hold <b>W</b> to move forward<br>
        Hold <b>A</b> to move to the left<br>
        Hold <b>S</b> to move backwards<br>
        Hold <b>D</b> to move to the right<br>
        Press <b>Ctrl</b> to grab/release mouse<br>`
        color: "white"
        styleColor: "black"
        style: Text.Outline
        font.pixelSize: 25
    }
}
