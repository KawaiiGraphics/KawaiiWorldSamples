import QtQuick 2.0

Item {
    anchors.fill: parent
    id: item

    Text {
        id: sampleText
        x: 5
        y: 5
        text: "Low brightness"
        color: "gray"
        styleColor: "black"
        style: Text.Outline
        font.pixelSize: 52

        MouseArea {
            anchors.fill: parent
            property bool bright: true

            onClicked: {
                if(!bright)
                {
                    sampleText.text = "High brightness"
                    sampleText.color = "yellow"
                    world.resources.exposure = 1.5
                    world.resources.gamma = 2.8
                } else
                {
                    sampleText.text = "Low brightness"
                    sampleText.color = "gray"
                    world.resources.exposure = 0.75
                    world.resources.gamma = 2.6
                }
                bright = !bright
            }
        }
    }

    Text {
        id: controlsText
        anchors.right: parent.right
        y: 5
        text: "Press <b>Ctrl</b> to grab/release mouse"
        color: "white"
        styleColor: "black"
        style: Text.Outline
        font.pixelSize: 25
    }
}
