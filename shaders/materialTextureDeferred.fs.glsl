module MaterialEffectFrag;
precision mediump int; precision highp float;

Material {
    sampler2D tex;
    sampler2D bloom;
} material;

layout(location = 2) in highp vec2 texCoord;

vec3 ownColor = vec3(0.0);
layout(location = 1) out highp vec3 gBufPos;
layout(location = 2) out highp vec3 gBufNormal;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec4 texel = texture(tex, texCoord.xy);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    ownColor = texel.rgb;
    gBufPos = worldPos;
    gBufNormal = (gl_FrontFacing)? normal: -normal;
}

export material_effect;
