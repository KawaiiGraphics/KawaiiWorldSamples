module MaterialEffectFrag;
precision mediump int; precision highp float;

input rgba32f gbuf_0;  // albedo
input rgba32f gbuf_1; // position
input rgba32f gbuf_2; // normal
input r16f gbuf_3; // ao

import MaterialInfo from PbrIlluminationCore;
MaterialInfo pbr_material;

void material_effect(out vec3 normal, out vec3 worldPos)
{
    pbr_material.albedo = subpassLoad(INPUT_GBUF_0).rgb;
    worldPos = subpassLoad(INPUT_GBUF_1).xyz;
    normal = subpassLoad(INPUT_GBUF_2).xyz;
    pbr_material.ao = pow(subpassLoad(INPUT_GBUF_3).r, 2.5);
}

export material_effect;
