module MaterialEffectFrag;
precision mediump int; precision highp float;
layout (depth_greater) out float gl_FragDepth;

Material {
    samplerCube tex;
} material;

layout(location = 2) in highp vec3 texCoord;

vec3 ownColor;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec4 texel = texture(tex, texCoord.xyz);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    ownColor = pow(texel.rgb, vec3(2.2));
}

export material_effect;
