module MaterialEffectFrag;
precision mediump int; precision highp float;

Material {
    sampler2D tex;
} material;

layout(location = 2) in highp vec2 texCoord;

import MaterialInfo from PbrIlluminationCore;

//albedo; metallic; roughness; ao;
MaterialInfo pbr_material;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec4 texel = texture(tex, texCoord.xy);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    pbr_material.albedo = texel.rgb;
}

export material_effect;
