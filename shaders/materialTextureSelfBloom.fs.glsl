module MaterialEffectFrag;
precision mediump int; precision highp float;

Material {
    sampler2D tex;
    sampler2D bloom;
} material;
layout(location = 2) in highp vec2 texCoord;

input rgba16f gbuf_0;  // previous bloom
vec3 ownColor = vec3(0);

void material_effect(in vec3 normal, in vec3 worldPos)
{
    ownColor = subpassLoad(INPUT_GBUF_0).rgb;
    vec4 texel = texture(bloom, texCoord.xy);
    ownColor += mix(texture(tex, texCoord.xy).rgb, 2.0*texel.rgb, texel.a);
}

export material_effect;
