module MaterialEffectVert;
precision mediump int; precision highp float;

layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in highp vec3 texcoord;

layout(location = 2) out highp vec3 texCoord;

void material_effect(in vec3 norm, in vec4 vert)
{
    texCoord = vert.xyz / 10.0;
}

export material_effect;
