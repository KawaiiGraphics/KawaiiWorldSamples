module MaterialEffectFrag;
precision mediump int; precision highp float;

input rgba32f gbuf_0;  // scene pixel
input rgba32f gbuf_1;  // bloom pixel
input r32f gbuf_2; // ao

layout(location = 0) out vec4 result;
void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec3 c = subpassLoad(INPUT_GBUF_0).rgb;
    c += subpassLoad(INPUT_GBUF_1).rgb * pow(subpassLoad(INPUT_GBUF_2).r, 2.5);

    c = c / (c + vec3(1.0));
    c = pow(c, vec3(1.0/2.2));
    result = vec4(c, 1.0);
}

export material_effect;
