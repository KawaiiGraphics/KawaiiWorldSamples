module MaterialEffectFrag;
precision mediump int; precision highp float;

input rgba32f gbuf_0;  // scene pixel

layout(location = 0) out vec4 result;
void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec3 c = subpassLoad(INPUT_GBUF_0).rgb;
    float brightness = dot(c, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0)
        result = vec4(2.0*c, 1.0);
    else
        result = vec4(0.0);
}

export material_effect;
